﻿namespace SUAS.Web.Areas.Training.Controllers
{
    using System;
    using Microsoft.AspNetCore.Mvc;
    using Models;
    using Services.Contracts;
    using ViewModels;

    [Area("Training")]
    public class TaskController : Controller
    {
        private readonly ICourseService courses;
        private readonly IGroupService groups;
        private readonly ITaskService tasks;

        public TaskController(ICourseService courses,
            IGroupService groups,
            ITaskService tasks)
        {
            this.courses = courses;
            this.groups = groups;
            this.tasks = tasks;
        }

        [HttpPost]
        public IActionResult CreateTask(int id, TaskViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                try
                {
                    var course = this.courses.GetById(id);
                    var group = this.groups.GetByName(model.ResponsibleGroupName);

                    var isCreated = this.tasks.CreateTask(group, model.Process, model.DueDate, course);

                    if (isCreated)
                    {
                        this.TempData["MessageState"] = "Success";
                        this.TempData["Message"] = "Successfully created new task!";
                    }
                }
                catch (ArgumentException ae)
                {
                    this.TempData["MessageState"] = "Error";
                    this.TempData["Message"] = ae.Message;
                }
            }

            return RedirectToAction("Show", "Course", new { id });
        }

        [HttpGet]
        public IActionResult CompleteTask(int id)
        {
            var task = this.tasks.GetById(id);
            if (task != null)
            {
                try
                {
                    var isCompleted = this.tasks.CompleteTask(task);

                    if (isCompleted)
                    {
                        this.TempData["MessageState"] = "Success";
                        this.TempData["Message"] = "Successfully completed task!";
                    }
                }
                catch (ArgumentException ae)
                {
                    this.TempData["MessageState"] = "Error";
                    this.TempData["Message"] = ae.Message;
                }
            }


            return RedirectToAction("Show", "Course", new { id = task.CourseId });
        }
    }
}
