﻿namespace SUAS.Web.Areas.Training.Controllers
{
    using System;
    using System.Linq;
    using Administration.ViewModels;
    using Microsoft.AspNetCore.Mvc;
    using Models;
    using Services.Contracts;
    using ViewModels;

    [Area("Training")]
    public class CourseController : Controller
    {
        private readonly ICourseService courses;
        private readonly ITaskService tasks;

        public CourseController(ICourseService courses,
            ITaskService tasks)
        {
            this.courses = courses;
            this.tasks = tasks;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var model = this.courses.All().Select(c => new CourseListViewModel
            {
                Id = c.Id,
                Name = c.Name,
                Credits = c.Credits,
                LectureWeeks = c.LectureWeeks,
                CourseModel = c.CourseModel,
                CourseType = c.CourseType
            }).ToList();

            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(CourseViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                try
                {
                    this.courses.CreateCourse(
                        model.Name,
                        model.CourseType,
                        model.CourseModel,
                        model.CapaignStartDate,
                        model.FirstLecture,
                        model.LastLecture,
                        model.ExamDate,
                        model.LectureWeeks,
                        model.ExamType,
                        model.Credits
                        );

                    return RedirectToAction("Index");
                }
                catch (ArgumentException ae)
                {
                    this.TempData["MessageState"] = "Error";
                    this.TempData["Message"] = ae.Message;
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Show(int id)
        {
            try
            {
                var course = this.courses.GetById(id);
                var tasksForCourse = this.tasks.GetAllTasksForCourse(id);

                var model = new CourseViewModel
                {
                    Id = course.Id,
                    Name = course.Name,
                    CourseType = course.CourseType,
                    CourseModel = course.CourseModel,
                    CapaignStartDate = course.CapaignStartDate,
                    FirstLecture = course.FirstLecture,
                    LastLecture = course.LastLecture,
                    ExamDate = course.ExamDate,
                    LectureWeeks = course.LectureWeeks,
                    ExamType = course.ExamType,
                    Credits = course.Credits,
                    Tasks = tasksForCourse
                };
                return View(model);
            }
            catch (ArgumentException ae)
            {
                this.TempData["MessageState"] = "Error";
                this.TempData["Message"] = ae.Message;
            }
            return RedirectToAction("Index");
        }
    }
}
