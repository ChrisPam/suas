﻿namespace SUAS.Web.Areas.Training.ViewModels
{
    using Models.Enums;

    public class CourseListViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int LectureWeeks { get; set; }
        public int Credits { get; set; }
        public CourseType CourseType { get; set; }
        public CourseModel CourseModel { get; set; }
    }
}
