﻿namespace SUAS.Web.Areas.Training.ViewModels
{
    using System;

    public class TaskViewModel
    {
        public string ResponsibleGroupName { get; set; }

        public string Process { get; set; }

        public DateTime DueDate { get; set; }
    }
}
