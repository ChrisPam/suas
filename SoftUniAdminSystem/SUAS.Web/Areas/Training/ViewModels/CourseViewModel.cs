﻿namespace SUAS.Web.Areas.Training.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.Serialization;
    using Models;
    using Models.Enums;

    public class CourseViewModel
    {
        public int Id { get; set; }
        [Required]
        [MinLength(10)]
        [Display(Prompt = "Course Name")]
        public string Name { get; set; }
        [Required]
        public CourseType CourseType { get; set; }
        [Required]
        public CourseModel CourseModel { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{MM/dd/yyyy}")]
        public DateTime CapaignStartDate { get; set; }

        public DateTime FirstLecture { get; set; }

        public DateTime LastLecture { get; set; }

        public DateTime ExamDate { get; set; }
        [Display(Prompt = "Lecture weeks")]
        public int LectureWeeks { get; set; }

        public ExamType ExamType { get; set; }
        [Display(Prompt = "Credits")]
        [Required]
        public int Credits { get; set; }
        public List<Task> Tasks { get; set; } = new List<Task>();
    }
}
