﻿namespace SUAS.Web.Areas.Administration.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    public class UserGroupViewModel
    {
        [Required]
        public int GroupId{ get; set; }
        [Required]
        public string UserName { get; set; }
    }
}
