﻿namespace SUAS.Web.Areas.Administration.ViewModels
{
    public class GroupListViewModel
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
        public int NumberOfUsers { get; set; }
        public int NumberOfChilds { get; set; }
    }
}
