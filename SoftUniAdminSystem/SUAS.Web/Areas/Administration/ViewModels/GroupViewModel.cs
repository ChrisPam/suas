﻿namespace SUAS.Web.Areas.Administration.ViewModels
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Models;

    public class GroupViewModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Group Name")]
        [MinLength(3)]
        public string GroupName { get; set; }
        public Group Parent { get; set; }
        public List<Group> Childs { get; set; }
        public ApplicationUser Leader { get; set; }
        [Required]
        [MinLength(3)]
        public string LeaderUserName { get; set; }
        public List<ApplicationUser> Participants { get; set; }
    }
}
