﻿namespace SUAS.Web.Areas.Administration.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    public class RoleViewModel
    {
        public string Id { get; set; }

        [Display(Name = "Role Name")]
        [Required]
        [MinLength(3)]
        public string RoleName { get; set; }
        public string Description { get; set; }
    }
}