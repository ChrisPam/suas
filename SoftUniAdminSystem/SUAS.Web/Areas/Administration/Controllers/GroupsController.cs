﻿namespace SUAS.Web.Areas.Administration.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Models;
    using Services.Contracts;
    using ViewModels;

    [Area("Administration")]
    [Authorize(Roles = "Administrator")]
    public class GroupsController : Controller
    {
        private readonly IGroupService groups;
        private readonly IUserService users;

        public GroupsController(IGroupService groups, IUserService users)
        {
            this.groups = groups;
            this.users = users;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var model = this.groups.All().Select(g => new GroupListViewModel
            {
                Id = g.GroupId,
                GroupName = g.Name,
                NumberOfUsers = this.groups.GetCountOfUsersFromGroup(g.GroupId),
                NumberOfChilds = this.groups.GetCountOfChildGroupsFromGroup(g.GroupId)
            }).ToList();

            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(GroupViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                try
                {
                    var groupLeader = this.users.GetUserByUserName(model.LeaderUserName);

                    this.groups.CreateGroup(model.GroupName, groupLeader);

                    return RedirectToAction("Index");
                }
                catch (ArgumentException ae)
                {
                    this.TempData["MessageState"] = "Error";
                    this.TempData["Message"] = ae.Message;
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Show(int id)
        {
            try
            {
                var group = this.groups.GetById(id);
                if (group.Parent == null)
                {
                    group.Parent = new Group { Name = "Top level group" };
                }
                var childGroups = this.groups.AllChildGroups(group.GroupId).ToList();

                var participants = this.users.All()
                    .Where(u => u.UserGroups
                        .Any(ug => ug.GroupId == group.GroupId)).ToList();

                var model = new GroupViewModel
                {
                    Id = group.GroupId,
                    GroupName = group.Name,
                    Parent = group.Parent,
                    Childs = childGroups,
                    Participants = participants,
                    LeaderUserName = group.Leader.UserName
                };
                return View(model);
            }
            catch (ArgumentException ae)
            {
                this.TempData["MessageState"] = "Error";
                this.TempData["Message"] = ae.Message;
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Delete(int? id)
        {
            if (id != null && id.Value > 0)
            {
                try
                {
                    this.groups.DeleteGroup(id.Value);
                }
                catch (ArgumentException ae)
                {
                    this.TempData["MessageState"] = "Error";
                    this.TempData["Message"] = ae.Message;
                }
            }

            return RedirectToAction("Index", "Groups");
        }

        [HttpPost]
        public IActionResult AddUserToGroup(UserGroupViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                try
                {
                    var user = this.users.GetUserByUserName(model.UserName);
                    var group = this.groups.GetById(model.GroupId);

                    this.groups.AddUserToGroup(group, user);
                }
                catch (ArgumentException ae)
                {
                    this.TempData["MessageState"] = "Error";
                    this.TempData["Message"] = ae.Message;
                }
            }
            return RedirectToAction("Show", new { id = model.GroupId });
        }

        public IActionResult RemoveUserFromGroup(UserGroupViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                try
                {
                    var user = this.users.GetUserByUserName(model.UserName);
                    var group = this.groups.GetById(model.GroupId);

                    this.groups.RemoveUserFromGroup(group, user);
                }
                catch (ArgumentException ae)
                {
                    this.TempData["MessageState"] = "Error";
                    this.TempData["Message"] = ae.Message;
                }
            }
            return RedirectToAction("Show", new { id = model.GroupId });
        }

        [HttpPost]
        public IActionResult AddChildGroup(int groupId, string groupName)
        {
            try
            {
                var parentGroup = this.groups.GetById(groupId);
                var childGroup = this.groups.GetByName(groupName);

                var isCreated = this.groups.AddChildGroup(parentGroup, childGroup);
            }
            catch (ArgumentException ae)
            {
                this.TempData["MessageState"] = "Error";
                this.TempData["Message"] = ae.Message;
            }

            return RedirectToAction("Show", new { id = groupId });
        }

        public IActionResult RemoveChildGroup(int groupId, string groupName)
        {
            try
            {
                var parentGroup = this.groups.GetById(groupId);
                var childGroup = this.groups.GetByName(groupName);

                this.groups.RemoveChildGroup(parentGroup, childGroup);
            }
            catch (ArgumentException ae)
            {
                this.TempData["MessageState"] = "Error";
                this.TempData["Message"] = ae.Message;
            }
            
            return RedirectToAction("Show", new { id = groupId });
        }

        [HttpGet]
        public List<string> GetAllUsers()
        {
            var allUsers = this.users.All().ToList();
            var usersUserNames = allUsers.Select(u => u.UserName).ToList();

            return usersUserNames;
        }

        [HttpGet]
        public List<string> GetAllGroups()
        {
            var allGroups = this.groups.All().ToList();
            var groupsNames = allGroups.Select(g => g.Name).ToList();

            return groupsNames;
        }
    }
}
