﻿namespace SUAS.Web.Areas.Administration.Controllers
{
    using System;
    using Microsoft.AspNetCore.Mvc;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Services.Contracts;
    using Models;
    using ViewModels;

    [Area("Administration")]
    [Authorize(Roles = "Administrator")]
    public class UsersController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<Role> roleManager;
        private readonly IUserService users;
        private readonly SignInManager<ApplicationUser> signInManager;

        public UsersController(UserManager<ApplicationUser> userManager,
                               RoleManager<Role> roleManager,
                               IUserService users,
                               SignInManager<ApplicationUser> signInManager)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.users = users;
            this.signInManager = signInManager;
        }

        public IActionResult Index()
        {
            var allUsers = this.users.All().Select(u => new UserListViewModel
            {
                Id = u.Id,
                UserName = u.UserName,
                Email = u.Email,
                RoleName = u.Position
            }).ToList();

            return View(allUsers);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new UserViewModel
            {
                ApplicationRoles = this.roleManager.Roles.Select(r => new SelectListItem
                {
                    Text = r.Name,
                    Value = r.Id
                }).ToList()
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Create(UserViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = model.UserName,
                    Email = model.Email
                };
                var result = await this.userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var applicationRole = await this.roleManager.FindByIdAsync(model.ApplicationRoleId);
                    if (applicationRole != null)
                    {
                        var roleResult = await this.userManager.AddToRoleAsync(user, applicationRole.Name);
                        if (roleResult.Succeeded)
                        {
                            this.TempData["MessageState"] = "Success";
                            this.TempData["Message"] = "Successfuly created user!";
                            return RedirectToAction("Index");
                        }
                    }
                }
                AddErrors(result);
            }
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(string id)
        {
            var model = new EditUserViewModel
            {
                ApplicationRoles = this.roleManager.Roles.Select(r => new SelectListItem
                {
                    Text = r.Name,
                    Value = r.Id
                }).ToList()
            };

            if (!String.IsNullOrEmpty(id))
            {
                var user = await this.userManager.FindByIdAsync(id);
                if (user != null)
                {
                    model.UserName = user.UserName;
                    model.Email = user.Email;
                    model.ApplicationRoleId = this.roleManager.Roles.Single(r => r.Name == this.userManager.GetRolesAsync(user).Result.Single()).Id;
                }
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(string id, EditUserViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);

                var user = await this.userManager.FindByIdAsync(id);
                if (user != null)
                {
                    user.UserName = model.UserName;
                    user.Email = model.Email;
                    var existingRole = this.userManager.GetRolesAsync(user).Result.Single();
                    var existingRoleId = this.roleManager.Roles.Single(r => r.Name == existingRole).Id;
                    var result = await this.userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        if (existingRoleId != model.ApplicationRoleId)
                        {
                            var roleResult = await this.userManager.RemoveFromRoleAsync(user, existingRole);
                            if (roleResult.Succeeded)
                            {
                                var applicationRole = await this.roleManager.FindByIdAsync(model.ApplicationRoleId);
                                if (applicationRole != null)
                                {
                                    var newRoleResult =
                                        await this.userManager.AddToRoleAsync(user, applicationRole.Name);
                                }
                            }
                        }

                        if (userId == model.Id)
                        {
                            await this.signInManager.SignOutAsync();
                            return Redirect("/Account/Login");
                        }
                        this.TempData["MessageState"] = "Success";
                        this.TempData["Message"] = "Successfuly edited user!";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }

            return View(id);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(string id)
        {
            var name = string.Empty;
            if (!String.IsNullOrEmpty(id))
            {
                var applicationUser = await this.userManager.FindByIdAsync(id);
                if (applicationUser != null)
                {
                    name = applicationUser.UserName;
                }
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id, IFormCollection form)
        {
            if (!String.IsNullOrEmpty(id))
            {
                var applicationUser = await this.userManager.FindByIdAsync(id);
                if (applicationUser != null)
                {
                    var result = await this.userManager.DeleteAsync(applicationUser);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Index");
                    }
                }
            }
            return RedirectToAction("Index");
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                this.ModelState.AddModelError(string.Empty, error.Description);
            }
        }
        #endregion Helpers
    }
}