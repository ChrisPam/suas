﻿namespace SUAS.Web.Areas.Administration.Controllers
{
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using System.Linq;
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Services.Contracts;
    using Models;
    using ViewModels;

    [Area("Administration")]
    [Authorize(Roles = "Administrator")]
    public class RolesController : Controller
    {
        private readonly RoleManager<Role> roleManager;
        private readonly IRoleService roles;

        public RolesController(RoleManager<Role> roleManager, IRoleService roles)
        {
            this.roleManager = roleManager;
            this.roles = roles;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var model = this.roles.GetAllRoles().Select(r => new RoleListViewModel
            {
                RoleName = r.Name,
                Id = r.Id,
                Description = r.Description,
                NumberOfUsers = this.roles.GetCountOfUsersInRole(r)
            }).ToList();

            return View(model);
        }

        [HttpGet]
        public IActionResult Create(string id)
        {
            var model = new RoleViewModel();
            if (!String.IsNullOrEmpty(id))
            {
                var role = this.roles.GetRoleById(id);
                if (role != null)
                {
                    model.Id = role.Id;
                    model.RoleName = role.Name;
                    model.Description = role.Description;
                }
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(string id, RoleViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var isExist = !String.IsNullOrEmpty(id);
                var role = isExist ? this.roles.GetRoleById(id) :
                    new Role
                    {
                        CreatedDate = DateTime.UtcNow
                    };

                role.Name = model.RoleName;
                role.Description = model.Description;

                var roleRuslt = isExist ? await this.roleManager.UpdateAsync(role)
                    : await this.roleManager.CreateAsync(role);
                if (roleRuslt.Succeeded)
                {
                    return RedirectToAction("Index");
                }
            }
            return View();
        }

        [HttpGet]
        public IActionResult Edit(string id)
        {
            var model = new RoleViewModel();
            if (!String.IsNullOrEmpty(id))
            {
                var role = this.roles.GetRoleById(id);
                if (role != null)
                {
                    model.Id = role.Id;
                    model.RoleName = role.Name;
                    model.Description = role.Description;
                }


            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(string id, RoleViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var role = this.roles.GetRoleById(id);

                role.Name = model.RoleName;
                role.Description = model.Description;

                var roleResult = await this.roleManager.UpdateAsync(role);
                if (roleResult.Succeeded)
                {
                    return RedirectToAction("Index");
                }

                this.TempData["MessageState"] = "Error";
                this.TempData["Message"] = $"Role {model.RoleName} already exists!";
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(string id)
        {
            var name = string.Empty;
            if (!String.IsNullOrEmpty(id))
            {
                var applicationRole = this.roles.GetRoleById(id);
                if (applicationRole != null)
                {
                    name = applicationRole.Name;
                }
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Delete(string id, IFormCollection form)
        {
            if (!String.IsNullOrEmpty(id))
            {
                var applicationRole = this.roles.GetRoleById(id);
                if (applicationRole != null)
                {
                    var roleRuslt = this.roleManager.DeleteAsync(applicationRole).Result;
                    if (roleRuslt.Succeeded)
                    {
                        return RedirectToAction("Index");
                    }
                }
            }
            return RedirectToAction("Index");
        }

        #region Helpers
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                this.ModelState.AddModelError(string.Empty, error.Description);
            }
        }
        #endregion Helpers
    }
}