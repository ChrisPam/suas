﻿namespace SUAS.Web.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    public class StatusCodeController : Controller
    {
        [HttpGet("/StatusCode/{statusCode}")]
        public IActionResult Index(int statusCode)
        {
            return View(statusCode);
        }
    }
}