﻿namespace SUAS.Web.Controllers
{
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authentication;
    using Models;
    using ViewModels.AccountViewModels;

    [Authorize]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly RoleManager<Role> roleManager;
        private readonly ILogger logger;

        private const string DefaultRoleName = "User";
        private const string DefaultAdminRoleName = "Administrator";

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            RoleManager<Role> roleManager,
            ILoggerFactory loggerFactory)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.roleManager = roleManager;
            this.logger = loggerFactory.CreateLogger<AccountController>();
        }

        // GET: /Account/Login
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            if (this.signInManager.IsSignedIn(this.User))
            {
                return RedirectToAction("Index", "Home");
            }

            // Clear the existing external cookie to ensure a clean login process
            await this.HttpContext.SignOutAsync();

            return View();
        }

        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            if (this.ModelState.IsValid)
            {
                var result = await this.signInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                this.ModelState.AddModelError(string.Empty, "Invalid login attempt.");
            }

            // Redisplay form
            return View(model);
        }

        // GET: /Account/Register
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            return View();
        }

        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.UserName, Email = model.Email };
                var result = await this.userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var userRole = await this.roleManager.FindByNameAsync(DefaultRoleName);
                    if (userRole == null)
                    {
                        userRole = new Role { Name = DefaultAdminRoleName };
                        await this.roleManager.CreateAsync(userRole);

                        var defaultUserRole = new Role { Name = DefaultRoleName };
                        await this.roleManager.CreateAsync(defaultUserRole);
                    }

                    var roleResult = await this.userManager.AddToRoleAsync(user, userRole.Name);
                    if (roleResult.Succeeded)
                    {
                        await this.signInManager.SignInAsync(user, false);
                        this.logger.LogInformation(3, "User created a new account with password.");
                        return RedirectToAction("Index", "Home");
                    }
                }
                AddErrors(result);
            }

            // Redisplay form
            return View(model);
        }

        // POST: /Account/Logout
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await this.signInManager.SignOutAsync();
            this.logger.LogInformation(4, "User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                this.ModelState.AddModelError(string.Empty, error.Description);
            }
        }
        #endregion Helpers
    }
}