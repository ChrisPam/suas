﻿namespace SUAS.Web.Controllers
{
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    [Authorize]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            this.HttpContext.Session.SetString("isCollapsedMode", "false");
            return View();
        }
    }
}