﻿namespace SUAS.Web.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Services.Contracts;
    using Models;

    public class SeedController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<Role> roleManager;
        private readonly IGroupService groups;
        private readonly IUserService users;

        private const string DefaultAdminRoleName = "Administrator";

        public SeedController(UserManager<ApplicationUser> userManager,
            RoleManager<Role> roleManager,
            IGroupService groups,
            IUserService users)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.groups = groups;
            this.users = users;
        }

        [AllowAnonymous]
        public async Task<IActionResult> Seed()
        {
            if (this.roleManager.Roles.ToList().Count != 0)
            {
                return RedirectToAction("Index", "Home");
            }
            var password = "1s2u3@4s56780-";
            var usersToImport = new Dictionary<string, string>
            {
                {"e.temelkova", "e.temelkova@softuni.bg"},
                {"k.pamidov", "k.pamidov@softuni.bg" },
                {"p.dzhigova", "p.dzhigova@softuni.bg"},
                {"m.hristoforova", "m.hristoforova@softuni.bg"},
                {"a.atanasova", "a.atanasova@softuni.bg"},
                {"a.terzieva", "a.terzieva@softuni.bg"},
                {"i.petrov", "i.petrov@softuni.bg"},
                {"a.svilarova", "a.svilarova@softuni.bg"},
                {"e.svilarova", "e.svilarova@softuni.bg"},
                {"d.georgieva", "d.georgieva@softuni.bg"},
                {"i.hristov", "i.hristov@softuni.bg"},
                {"r.nedyalkova", "r.nedyalkova@softuni.bg"},
                {"j.tahova", "j.tahova@softuni.bg"},
                {"a.borisov", "a.borisov@softuni.bg"},
                {"i.yonkov", "i.yonkov@softuni.bg"},
                {"a.ivanov", "a.ivanov@softuni.bg"},
                {"r.georgieva", "r.georgieva@softuni.bg"},
                {"s.nakov", "s.nakov@softuni.bg"},
                {"i.kenov", "i.kenov@softuni.bg"},
                {"b.gevechanov", "b.gevechanov@softuni.bg"},
                {"i.ivanov", "i.ivanov@softuni.bg"},
                {"v.petrov", "v.petrov@softuni.bg"},
                {"r.nenova", "r.nenova@softuni.bg"},
                {"v.kostadinov", "v.kostadinov@softuni.bg"},
                {"s.sheytanov", "s.sheytanov@softuni.bg"},
                {"i.zhelev", "i.zhelev@softuni.bg"},
                {"p.penev", "p.penev@softuni.bg"},
                {"k.kirilov", "k.kirilov@softuni.bg"},
                {"v.ivanov", "v.ivanov@softuni.bg"},
                {"v.dimitrov", "v.dimitrov@softuni.bg"},
                {"v.damyanovski", "v.damyanovski@softuni.bg"},
                {"v.peevski", "v.peevski@softuni.bg"},
                {"is.ivanov", "is.ivanov@softuni.bg"},
                {"t.apov", "t.apov@softuni.bg"},
                {"a.alexandrov", "a.alexandrov@softuni.bg"},
                {"s.gerova", "s.gerova@softuni.bg"},
                {"s.stefanova", "s.stefanova@softuni.bg"},
                {"l.petrova", "l.petrova@softuni.bg"},
                {"t.manchev", "t.manchev@softuni.bg"},
                {"m.roshlev", "m.roshlev@softuni.bg"},
                {"y.slavcheva", "y.slavcheva@softuni.bg"},
                {"r.popova", "r.popova@softuni.bg"},
                {"v.chalakova", "v.chalakova@softuni.bg"},
                {"v.kazakov", "v.kazakov@softuni.bg"},
                {"s.galov", "s.galov@softuni.bg"},
                {"t.tsenov", "t.tsenov@softuni.bg"},
                {"y.asenov", "y.asenov@softuni.bg"},
                {"g.georgiev", "g.georgiev@softuni.bg"},
                {"a.georgiev", "a.georgiev@softuni.bg"}
            };

            foreach (var user in usersToImport)
            {
                try
                {
                    if (this.users.GetUserByUserName(user.Key) == null)
                    {
                        var userToImport = new ApplicationUser { UserName = user.Key, Email = user.Value };
                        var result = await this.userManager.CreateAsync(userToImport, password);
                        if (result.Succeeded)
                        {
                            var applicationRole = await this.roleManager.FindByNameAsync(DefaultAdminRoleName);

                            if (applicationRole == null)
                            {
                                applicationRole = new Role { Name = DefaultAdminRoleName };
                                await this.roleManager.CreateAsync(applicationRole);
                            }
                            var roleResult = await this.userManager.AddToRoleAsync(userToImport, applicationRole.Name);
                        }
                    }
                }
                catch
                {
                    continue;
                }

            }

            var groupsToImport = new Dictionary<string, string>
            {
                {"Marketing", "e.temelkova"},
                {"CRM", "a.svilarova"},
                {"Human Resources", "r.nedyalkova"},
                {"Business", "a.borisov"},
                {"Training", "i.nenkov"},
                {"Senior Trainers", "i.yonkov"},
                {"Training Leads", "i.yonkov"},
                {"Technical Writers", "s.nakov"},
                {"Team Leads", "i.yonkov"},
                {"Tech Supports", "is.ivanov"},
                {"Developers", "y.slavcheva"},
                {"R&D", "a.georgiev"}
            };

            foreach (var group in groupsToImport)
            {
                try
                {
                    if (this.groups.GetByName(group.Key) == null)
                    {
                        var leader = this.users.GetUserByUserName(group.Value);
                        if (leader != null)
                        {
                            this.groups.CreateGroup(group.Key, leader);
                        }
                    }
                }
                catch
                {
                    continue;
                }
            }

            this.ViewData["MessageState"] = "Success";
            this.ViewData["Message"] = "Successfully imported data!";

            return this.RedirectToAction("Index", "Home");
        }
    }
}