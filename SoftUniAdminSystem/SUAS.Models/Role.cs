﻿namespace SUAS.Models
{
    using System;
    using Microsoft.AspNetCore.Identity;

    public class Role : IdentityRole
    {
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
