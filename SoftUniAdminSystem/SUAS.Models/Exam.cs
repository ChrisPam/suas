﻿namespace SUAS.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Exam
    {
        public int Id { get; set; }
        [Required]
        [MinLength(20)]
        public string Name { get; set; }
        [Required]
        [MinLength(20)]
        public string Description { get; set; }
        public List<Question> Questions { get; set; } = new List<Question>();
    }
}