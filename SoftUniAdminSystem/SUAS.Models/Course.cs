﻿namespace SUAS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Enums;

    public class Course
    {
        public int Id { get; set; }
        [Required]
        [MinLength(10)]
        public string Name { get; set; }
        [Required]
        public CourseType CourseType { get; set; }
        [Required]
        public CourseModel CourseModel { get; set; }

        public DateTime CapaignStartDate { get; set; }

        public DateTime FirstLecture { get; set; }

        public DateTime LastLecture { get; set; }

        public DateTime ExamDate { get; set; }

        public int LectureWeeks { get; set; }

        public ExamType ExamType { get; set; }

        public int Credits { get; set; }

        public List<Task> Tasks { get; set; } = new List<Task>();
    }
}
