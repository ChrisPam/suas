﻿namespace SUAS.Models.Enums
{
    public enum CourseModel
    {
        Free = 0,
        Paid = 1
    }
}
