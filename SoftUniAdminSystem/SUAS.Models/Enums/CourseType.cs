﻿namespace SUAS.Models.Enums
{
    public enum CourseType
    {
        Open = 0,
        Internal = 1
    }
}
