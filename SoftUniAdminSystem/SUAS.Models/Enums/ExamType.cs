﻿namespace SUAS.Models.Enums
{
    using System.ComponentModel.DataAnnotations;

    public enum ExamType
    {
        Practical = 0,
        [Display(Name = "Test system")]
        TestSystem = 1,
        [Display(Name = "Judge exam")]
        JudgeExam = 2,
        Project = 3,
        [Display(Name = "Practical and test system")]
        PracticalAndTestSystem = 4,
    }
}
