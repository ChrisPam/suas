﻿namespace SUAS.Models
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Identity;

    public class ApplicationUser : IdentityUser
    {
        public string ProfilePictureName { get; set; } = "default.png";
        public string Position { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public List<UserGroup> UserGroups { get; set; } = new List<UserGroup>();
    }
}