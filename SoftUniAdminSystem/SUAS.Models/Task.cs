﻿namespace SUAS.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class Task
    {
        public int Id { get; set; }
        [Required]
        public Group ResponsibleGroup { get; set; }

        public int ResponsibleGroupId { get; set; }
        [Required]
        public Course Course { get; set; }

        public int CourseId { get; set; }
        [MinLength(10)]
        public string Process { get; set; }
        
        public DateTime DueDate { get; set; }

        public bool Completed { get; set; }
        
        public DateTime CreatedOn { get; set; } = DateTime.Now;

        public DateTime CompletedOn { get; set; }
    }
}
