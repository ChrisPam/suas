﻿namespace SUAS.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Group
    {
        public int GroupId { get; set; }
        [Required]
        [MinLength(3)]
        public string Name { get; set; }
        [Required]
        public ApplicationUser Leader { get; set; }
        public int LeaderId { get; set; }
        public Group Parent { get; set; }

        public List<Group> ChildGroups { get; set; } = new List<Group>();
        public List<UserGroup> UserGroups { get; set; } = new List<UserGroup>();
    }
}
