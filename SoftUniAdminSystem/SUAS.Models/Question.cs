﻿namespace SUAS.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Enums;

    public class Question
    {
        public int Id { get; set; }
        public Exam Exam { get; set; }
        public int ExamId { get; set; }
        [MinLength(30)]
        public string Text { get; set; }
        public ExamType Type { get; set; }
        public List<Answer> Answers { get; set; } = new List<Answer>();
    }
}