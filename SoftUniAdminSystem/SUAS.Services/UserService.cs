﻿namespace SUAS.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using Contracts;
    using Data;
    using Microsoft.EntityFrameworkCore;
    using Models;

    public class UserService : IUserService
    {
        private readonly SuasDbContext context;

        public UserService(SuasDbContext context)
        {
            this.context = context;
        }

        public List<ApplicationUser> All()
            => this.context
                .Users
                .Include(u => u.UserGroups)
                .AsQueryable()
                .OrderBy(u => u.CreatedOn)
                .ToList();

        public ApplicationUser GetUserByUserName(string userName)
            => this.context.Users
                .FirstOrDefault(u => u.UserName.ToLower() == userName.ToLower());
    }
}
