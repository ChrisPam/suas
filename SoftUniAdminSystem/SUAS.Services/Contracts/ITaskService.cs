﻿namespace SUAS.Services.Contracts
{
    using System;
    using System.Collections.Generic;
    using Models;

    public interface ITaskService
    {
        bool CreateTask(Group responsibleGroup,
            string process,
            DateTime dueDate,
            Course course);

        List<Task> GetAllTasksForCourse(int id);

        bool CompleteTask(Task taskToComplete);

        Task GetById(int id);
    }
}
