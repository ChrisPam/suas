﻿namespace SUAS.Services.Contracts
{
    using System.Collections.Generic;
    using Models;

    public interface IRoleService
    {
        List<Role> GetAllRoles();
        Role GetRoleById(string id);
        int GetCountOfUsersInRole(Role role);
    }
}
