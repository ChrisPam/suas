﻿namespace SUAS.Services.Contracts
{
    using System.Collections;
    using System.Collections.Generic;
    using Models;

    public interface IUserService
    {
        ApplicationUser GetUserByUserName(string userName);
        List<ApplicationUser> All();
    }
}
