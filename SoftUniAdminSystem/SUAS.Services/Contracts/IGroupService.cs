﻿namespace SUAS.Services.Contracts
{
    using System.Collections.Generic;
    using Models;

    public interface IGroupService
    {
        List<Group> All();
        List<Group> AllChildGroups(int groupId);
        Group GetById(int groupId);
        Group GetByName(string groupName);
        int GetCountOfUsersFromGroup(int groupId);
        int GetCountOfChildGroupsFromGroup(int groupId);
        bool CreateGroup(string groupName, ApplicationUser groupLeader);
        bool AddUserToGroup(Group group, ApplicationUser user);
        bool RemoveUserFromGroup(Group group, ApplicationUser user);
        bool AddChildGroup(Group parentGroup, Group childGroup);
        bool RemoveChildGroup(Group parentGroup, Group childGroup);
        bool DeleteGroup(int id);
    }
}
