﻿namespace SUAS.Services.Contracts
{
    using System;
    using System.Collections.Generic;
    using Models;
    using Models.Enums;

    public interface ICourseService
    {
        List<Course> All();

        bool CreateCourse(string courseName,
            CourseType courseType,
            CourseModel courseModel,
            DateTime campaignStartDate,
            DateTime firstLecture,
            DateTime lastLecture,
            DateTime examDate,
            int lectureWeeks,
            ExamType examType,
            int credits);

        Course GetById(int id);
    }
}
