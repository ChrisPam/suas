﻿namespace SUAS.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Contracts;
    using Data;
    using Microsoft.EntityFrameworkCore;
    using Models;
    using Models.Enums;

    public class CourseService : ICourseService
    {
        private readonly SuasDbContext context;

        public CourseService(SuasDbContext context)
        {
            this.context = context;
        }

        public List<Course> All()
            => this.context
                .Courses
                .AsQueryable()
                .OrderBy(c => c.Id)
                .ToList();

        public bool CreateCourse(string courseName,
                                CourseType courseType,
                                CourseModel courseModel,
                                DateTime campaignStartDate,
                                DateTime firstLecture,
                                DateTime lastLecture,
                                DateTime examDate,
                                int lectureWeeks,
                                ExamType examType,
                                int credits)
        {
            var course = new Course
            {
                Name = courseName,
                CourseType = courseType,
                CourseModel = courseModel,
                CapaignStartDate = campaignStartDate,
                FirstLecture = firstLecture,
                LastLecture = lastLecture,
                ExamDate = examDate,
                LectureWeeks = lectureWeeks,
                ExamType = examType,
                Credits = credits
            };

            if (this.context.Courses.Count(c => c.Name.ToLower() == courseName.ToLower()) > 0)
            {
                throw new ArgumentException("Course with that name already exists!");
            }

            this.context.Courses.Add(course);
            this.context.SaveChanges();

            return true;
        }

        public Course GetById(int id)
            => this.context
                .Courses
                .FirstOrDefault(c => c.Id== id);
    }
}
