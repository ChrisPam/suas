﻿namespace SUAS.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Contracts;
    using Data;
    using Microsoft.EntityFrameworkCore;
    using Models;

    public class GroupService : IGroupService
    {
        private readonly SuasDbContext context;

        public const string UserNotExists = "User doesn't exists!";
        public const string GroupNotExists = "Group doesn't exsts!";

        public GroupService(SuasDbContext context)
        {
            this.context = context;
        }
        public List<Group> All()
            => this.context
                .Groups
                .AsQueryable()
                .OrderBy(g => g.GroupId)
                .ToList();

        public List<Group> AllChildGroups(int groupId)
            => this.context
                .Groups
                .Where(g => g.Parent.GroupId == groupId)
                .AsQueryable()
                .OrderBy(g => g.Name)
                .ToList();

        public Group GetById(int groupId)
            => this.context
                .Groups
                .Include(g => g.Leader)
                .Include(g => g.UserGroups)
                .Include(g => g.Parent)
                .FirstOrDefault(g => g.GroupId == groupId);

        public Group GetByName(string groupName)
            => this.context
                .Groups
                .Include(g => g.Leader)
                .Include(g => g.UserGroups)
                .FirstOrDefault(g => g.Name == groupName);

        public int GetCountOfUsersFromGroup(int groupId)
            => this.context
                   .UserGroups
                   .Count(ug => ug.GroupId == groupId);

        public int GetCountOfChildGroupsFromGroup(int groupId)
            => this.All()
                   .Count(g => g.Parent != null && g.Parent.GroupId == groupId);

        public bool CreateGroup(string groupName, ApplicationUser groupLeader)
        {
            var group = new Group()
            {
                Name = groupName,
                Leader = groupLeader
            };

            if (string.IsNullOrEmpty(group.Name))
            {
                throw new ArgumentException("Name can't be empty!");
            }

            if (groupLeader == null)
            {
                throw new ArgumentException(UserNotExists);
            }

            if (this.context.Groups.Count(g => g.Name.ToLower() == groupName.ToLower()) > 0)
            {
                throw new ArgumentException("Group with that name already exists!");
            }

            this.context.Groups.Add(group);
            this.context.SaveChanges();

            return true;
        }

        public bool AddUserToGroup(Group group, ApplicationUser user)
        {
            if (group == null)
            {
                throw new ArgumentException(GroupNotExists);
            }

            if (user == null)
            {
                throw new ArgumentException(UserNotExists);
            }

            var userGroup = new UserGroup
            {
                Group = group,
                User = user
            };

            if (this.context.UserGroups.Count(ug => ug.Group == group && ug.User == user) > 0)
            {
                throw new ArgumentException("User is already in the group!");
            }

            this.context.UserGroups.Add(userGroup);
            this.context.SaveChanges();

            return true;
        }

        public bool RemoveUserFromGroup(Group group, ApplicationUser user)
        {
            if (group == null)
            {
                throw new ArgumentException(GroupNotExists);
            }

            if (user == null)
            {
                throw new ArgumentException(UserNotExists);
            }

            var userGroupToRemove = this.context.UserGroups.FirstOrDefault(ug => ug.Group == group && ug.User == user);

            if (userGroupToRemove == null)
            {
                throw new ArgumentException("User doesn't exists in that group!");
            }

            user.UserGroups.Remove(userGroupToRemove);
            this.context.SaveChanges();

            return true;
        }

        public bool AddChildGroup(Group parentGroup, Group childGroup)
        {
            if (parentGroup == null)
            {
                throw new ArgumentException("Parent group doesn't exists!");
            }

            if (childGroup == null)
            {
                throw new ArgumentException("Child group doesn't exists!");
            }

            parentGroup.ChildGroups.Add(childGroup);
            childGroup.Parent = parentGroup;
            this.context.SaveChanges();

            return true;
        }

        public bool RemoveChildGroup(Group parentGroup, Group childGroup)
        {
            if (parentGroup == null)
            {
                throw new ArgumentException("Parent group doesn't exists!");
            }

            if (childGroup == null)
            {
                throw new ArgumentException("Child group doesn't exists!");
            }

            if (parentGroup.ChildGroups.Count(g => g.Equals(childGroup)) <= 0)
            {
                throw new ArgumentException("This group doesn't have child group with this name!");
            }

            parentGroup.ChildGroups.Remove(childGroup);
            this.context.SaveChanges();

            return true;
        }

        public bool DeleteGroup(int id)
        {
            var groupToDelete = this.GetById(id);
            if (groupToDelete == null)
            {
                throw new ArgumentException("Group doesn't exists!");
            }

            this.context.Groups.Remove(groupToDelete);
            this.context.SaveChanges();

            return true;
        }
    }
}
