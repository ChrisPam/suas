﻿namespace SUAS.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Contracts;
    using Data;
    using Microsoft.EntityFrameworkCore;
    using Models;

    public class TaskService : ITaskService
    {
        private readonly SuasDbContext context;

        public TaskService(SuasDbContext context)
        {
            this.context = context;
        }

        public bool CreateTask(Group responsibleGroup, string process, DateTime dueDate, Course course)
        {
            var task = new Task
            {
                Completed = false,
                Course = course,
                CreatedOn = DateTime.Now,
                DueDate = dueDate,
                Process = process,
                ResponsibleGroup = responsibleGroup
            };

            this.context.Tasks.Add(task);
            this.context.SaveChanges();

            return true;
        }

        public Task GetById(int id)
            => this.context
                .Tasks
                .FirstOrDefault(t => t.Id == id);

        public List<Task> GetAllTasksForCourse(int id)
            => this.context
                .Tasks
                    .Include(t => t.ResponsibleGroup)
                .Where(t => t.CourseId == id)
                .ToList();

        public bool CompleteTask(Task taskToComplete)
        {
            taskToComplete.Completed = true;
            taskToComplete.CompletedOn = DateTime.Now;

            this.context.SaveChanges();
            return true;
        }
    }
}
