﻿namespace SUAS.Services
{
    using System.Linq;
    using Contracts;
    using Data;
    using Models;
    using System.Collections.Generic;

    public class RoleService : IRoleService
    {
        private readonly SuasDbContext context;

        public RoleService(SuasDbContext context)
        {
            this.context = context;
        }

        public List<Role> GetAllRoles()
            => this.context
                .Roles
                .AsQueryable()
                .OrderBy(r => r.CreatedDate)
                .ToList();

        public int GetCountOfUsersInRole(Role role)
            => this.context
                .UserRoles
                .Count(ur => ur.RoleId == role.Id);

        public Role GetRoleById(string id)
            => this.context
                .Roles
                .FirstOrDefault(r => r.Id == id);
    }
}
