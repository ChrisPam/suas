﻿namespace SUAS.Tests.Services
{
    using System;
    using System.Collections.Generic;
    using Data;
    using Microsoft.EntityFrameworkCore;
    using Models;
    using SUAS.Services;
    using Xunit;

    public class UserServiceTest
    {
        [Fact]
        public void GetAllUsersShouldOrderByCreatedOn()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            var db = new SuasDbContext(dbOptions);

            var firstUser = new ApplicationUser
            {
                UserName = "Pesho",
                CreatedOn = DateTime.Now.AddHours(1)
            };

            var secondUser = new ApplicationUser
            {
                UserName = "Gosho",
                CreatedOn = DateTime.Now
            };

            db.Users.Add(firstUser);
            db.Users.Add(secondUser);
            db.SaveChanges();

            var userService = new UserService(db);

            // Act
            var allUsers = userService.All();

            // Assert
            Assert.Equal(firstUser, allUsers[1]);
            Assert.Equal(secondUser, allUsers[0]);
        }

        [Fact]
        public void GetUserByUserNameShouldReturnOneUser()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            var db = new SuasDbContext(dbOptions);

            var group = new Group
            {
                Name = "SoftUni"
            };

            var user = new ApplicationUser
            {
                UserName = "Pesho",
                UserGroups = new List<UserGroup>
                {
                    new UserGroup
                    {
                        Group = group
                    }
                }
            };

            db.Users.Add(user);
            db.SaveChanges();

            var userService = new UserService(db);

            // Act
            var findedUser = userService.GetUserByUserName("Pesho");

            // Assert
            Assert.NotNull(findedUser);
            Assert.NotNull(findedUser.UserGroups);
        }
    }
}
