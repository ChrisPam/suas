﻿namespace SUAS.Tests.Services
{
    using System;
    using System.Collections.Generic;
    using Data;
    using Microsoft.EntityFrameworkCore;
    using Models;
    using SUAS.Services;
    using Xunit;

    public class GroupServiceTest
    {
        public const string UserNotExists = "User doesn't exists!";
        public const string GroupNotExists = "Group doesn't exsts!";

        [Fact]
        public void GetAllGroupsShouldOrderById()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            var db = new SuasDbContext(dbOptions);

            var firstGroup = new Group
            {
                GroupId = 2,
                Name = "B"
            };
            var secondGroup = new Group
            {
                GroupId = 1,
                Name = "A"
            };

            var group = new Group
            {
                GroupId = 3,
                Name = "SoftUni",
                ChildGroups = new List<Group>
                {
                    firstGroup,
                    secondGroup
                }
            };

            db.Groups.Add(group);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            var childGroups = groupService.All();

            // Assert
            Assert.Equal(firstGroup, childGroups[1]);
            Assert.Equal(secondGroup, childGroups[0]);
        }

        [Fact]
        public void GetAllChildGroupsShouldOrderByName()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);

            var firstGroup = new Group
            {
                Name = "B"
            };
            var secondGroup = new Group
            {
                Name = "A"
            };

            var group = new Group
            {
                Name = "SoftUni",
                ChildGroups = new List<Group>
                {
                    firstGroup,
                    secondGroup
                }
            };

            db.Groups.Add(group);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            var childGroups = groupService.AllChildGroups(group.GroupId);

            // Assert
            Assert.Equal(firstGroup, childGroups[1]);
            Assert.Equal(secondGroup, childGroups[0]);
        }

        [Fact]
        public void GetByIdShouldReturnGroupWithLeaderAndUserGroups()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);
            var user = new ApplicationUser
            {
                UserName = "Pesho"
            };

            var secondUser = new ApplicationUser
            {
                UserName = "Pesho"
            };

            var secondGroup = new Group
            {
                Name = "Parent"
            };

            var group = new Group
            {
                Name = "SoftUni",
                UserGroups = new List<UserGroup>
                {
                    new UserGroup
                    {
                        User = user
                    },
                    new UserGroup
                    {
                        User = secondUser
                    }
                },
                Leader = user,
                Parent = secondGroup
            };

            db.Groups.Add(group);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            var findedGroup = groupService.GetById(group.GroupId);

            // Assert
            Assert.NotNull(findedGroup.UserGroups);
            Assert.NotNull(findedGroup.Leader);
        }

        [Fact]
        public void GetByNameShouldReturnGroupWithLeaderAndUserGroups()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            var db = new SuasDbContext(dbOptions);
            var user = new ApplicationUser
            {
                UserName = "Pesho"
            };

            var secondUser = new ApplicationUser
            {
                UserName = "Pesho"
            };

            var secondGroup = new Group
            {
                Name = "Parent"
            };

            var group = new Group
            {
                Name = "SoftUni",
                UserGroups = new List<UserGroup>
                {
                    new UserGroup
                    {
                        User = user
                    },
                    new UserGroup
                    {
                        User = secondUser
                    }
                },
                Leader = user,
                Parent = secondGroup
            };

            db.Groups.Add(group);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            var findedGroup = groupService.GetByName("SoftUni");

            // Assert
            Assert.NotNull(findedGroup.UserGroups);
            Assert.NotNull(findedGroup.Leader);
        }

        [Fact]
        public void GetCountOfUsersFromGroup()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);
            var user = new ApplicationUser
            {
                UserName = "Pesho"
            };

            var secondUser = new ApplicationUser
            {
                UserName = "Pesho"
            };

            var group = new Group
            {
                Name = "SoftUni",
                UserGroups = new List<UserGroup>
                {
                    new UserGroup
                    {
                        User = user
                    },
                    new UserGroup
                    {
                        User = secondUser
                    }
                }
            };

            db.Groups.Add(group);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            var countOfUsers = groupService.GetCountOfUsersFromGroup(group.GroupId);

            // Assert
            Assert.True(countOfUsers == 2);
        }

        [Fact]
        public void GetCountOfChildGroupsFromGroup()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);
            var childGroup = new Group
            {
                Name = "Training"
            };

            var childGroup2 = new Group
            {
                Name = "Development"
            };

            var group = new Group
            {
                Name = "SoftUni",
                ChildGroups = new List<Group>
                {
                    childGroup,
                    childGroup2
                }
            };

            db.Groups.Add(group);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            var countOfChildGroups = groupService.GetCountOfChildGroupsFromGroup(group.GroupId);

            // Assert
            Assert.True(countOfChildGroups == 2);
        }

        [Fact]
        public void CreateGroupWithNullLeaderShouldThrowAnException()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);

            var groupService = new GroupService(db);

            // Act
            void Act() => groupService.CreateGroup("Training", null);

            // Assert
            var exception = Assert.Throws<ArgumentException>((Action)Act);
            Assert.Equal(UserNotExists, exception.Message);
        }

        [Fact]
        public void CreateGroupWithEmptyNameShouldThrowAnException()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);
            var groupLeader = new ApplicationUser
            {
                Email = "test@gmail.com",
                UserName = "test"
            };

            db.Users.Add(groupLeader);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            void Act() => groupService.CreateGroup(null, groupLeader);

            // Assert
            var exception = Assert.Throws<ArgumentException>((Action)Act);
            Assert.Equal("Name can't be empty!", exception.Message);
        }

        [Fact]
        public void CreateGroupWithCorrectParametersShouldPopulateInDatabase()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);
            var groupLeader = new ApplicationUser
            {
                Email = "test@gmail.com",
                UserName = "test"
            };

            db.Users.Add(groupLeader);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            var isCreated = groupService.CreateGroup("Training", groupLeader);

            // Assert
            Assert.True(isCreated);
        }

        [Fact]
        public void AddUserToGroupWithNullGroupShouldThrowAnException()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);
            var user = new ApplicationUser
            {
                Email = "test@gmail.com",
                UserName = "test"
            };

            db.Users.Add(user);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            void Act() => groupService.AddUserToGroup(null, user);

            // Assert
            var exception = Assert.Throws<ArgumentException>((Action)Act);
            Assert.Equal(GroupNotExists, exception.Message);
        }

        [Fact]
        public void AddUserToGroupWithNullUserShouldThrowAndException()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);

            var group = new Group
            {
                Name = "Training"
            };

            db.Groups.Add(group);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            void Act() => groupService.AddUserToGroup(group, null);

            // Assert
            var exception = Assert.Throws<ArgumentException>((Action)Act);
            Assert.Equal(UserNotExists, exception.Message);
        }

        [Fact]
        public void AddUserToGroupWithCorrectDataShouldPopulateInDatabase()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);
            var user = new ApplicationUser
            {
                Email = "test@gmail.com",
                UserName = "test"
            };

            var group = new Group
            {
                Name = "Training"
            };

            db.Users.Add(user);
            db.Groups.Add(group);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            var isUserAddedToGroup = groupService.AddUserToGroup(group, user);

            // Assert
            Assert.True(isUserAddedToGroup);
        }

        [Fact]
        public void AddUserToGroupWhenUserIsAlreadyInGroupShouldThrowAnException()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);
            var user = new ApplicationUser
            {
                Email = "test@gmail.com",
                UserName = "test"
            };

            var group = new Group
            {
                Name = "Training"
            };

            db.Users.Add(user);
            db.Groups.Add(group);
            db.SaveChanges();

            var groupService = new GroupService(db);
            groupService.AddUserToGroup(group, user);

            // Act
            void Act() => groupService.AddUserToGroup(group, user);

            // Assert
            var exception = Assert.Throws<ArgumentException>((Action)Act);
            Assert.Equal("User is already in the group!", exception.Message);
        }

        [Fact]
        public void RemoveUserFromGroupWithNullGroupShouldThrowAnException()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);
            var user = new ApplicationUser
            {
                Email = "test@gmail.com",
                UserName = "test"
            };

            db.Users.Add(user);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            void Act() => groupService.RemoveUserFromGroup(null, user);

            // Assert
            var exception = Assert.Throws<ArgumentException>((Action)Act);
            Assert.Equal(GroupNotExists, exception.Message);
        }

        [Fact]
        public void RemoveUserFromGroupWithNullUserShouldThrowAnException()
        {
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);

            var group = new Group
            {
                Name = "Training"
            };

            db.Groups.Add(group);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            void Act() => groupService.RemoveUserFromGroup(group, null);

            // Assert
            var exception = Assert.Throws<ArgumentException>((Action)Act);
            Assert.Equal(UserNotExists, exception.Message);
        }

        [Fact]
        public void RemoveUserFromGroupWithoutUserIsInGroupShouldThrowAnException()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);
            var user = new ApplicationUser
            {
                Email = "test@gmail.com",
                UserName = "test"
            };

            var group = new Group
            {
                Name = "Training"
            };

            db.Users.Add(user);
            db.Groups.Add(group);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            void Act() => groupService.RemoveUserFromGroup(group, user);

            // Assert
            var exception = Assert.Throws<ArgumentException>((Action)Act);
            Assert.Equal("User doesn't exists in that group!", exception.Message);
        }

        [Fact]
        public void RemoveUserFromGroupWithCorrectDataShouldRemoveUserFromDatabase()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);
            var user = new ApplicationUser
            {
                Email = "test@gmail.com",
                UserName = "test"
            };

            var group = new Group
            {
                Name = "Training"
            };

            var userGroup = new UserGroup
            {
                Group = group,
                User = user
            };

            db.Users.Add(user);
            db.Groups.Add(group);
            db.UserGroups.Add(userGroup);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            var isUserRemovedFromGroup = groupService.RemoveUserFromGroup(group, user);

            // Assert
            Assert.True(isUserRemovedFromGroup);
        }

        [Fact]
        public void AddChildGroupWithNullParentGroupShouldThrowAnException()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);

            var childGroup = new Group
            {
                Name = "Training"
            };

            db.Groups.Add(childGroup);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            void Act() => groupService.AddChildGroup(null, childGroup);

            // Assert
            var exception = Assert.Throws<ArgumentException>((Action)Act);
            Assert.Equal("Parent group doesn't exists!", exception.Message);
        }

        [Fact]
        public void AddChildGroupWithNullChildGroupShouldThrowAnException()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);
            var parentGroup = new Group
            {
                Name = "SoftUni"
            };

            db.Groups.Add(parentGroup);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            void Act() => groupService.AddChildGroup(parentGroup, null);

            // Assert
            var exception = Assert.Throws<ArgumentException>((Action)Act);
            Assert.Equal("Child group doesn't exists!", exception.Message);
        }

        [Fact]
        public void AddChildGroupWithCorrectDataShouldPopulateInDatabase()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);
            var parentGroup = new Group
            {
                Name = "SoftUni"
            };

            var childGroup = new Group
            {
                Name = "Training"
            };

            db.Groups.Add(parentGroup);
            db.Groups.Add(childGroup);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            var isChildGroupAddedToParentGroup = groupService.AddChildGroup(parentGroup, childGroup);

            // Assert
            Assert.True(isChildGroupAddedToParentGroup);
        }

        [Fact]
        public void RemoveChildGroupWithNullParentGroupShouldThrowAnException()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);
            var parentGroup = new Group
            {
                Name = "SoftUni"
            };

            var childGroup = new Group
            {
                Name = "Training"
            };

            db.Groups.Add(parentGroup);
            db.Groups.Add(childGroup);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            void Act() => groupService.RemoveChildGroup(null, childGroup);

            // Assert
            var exception = Assert.Throws<ArgumentException>((Action)Act);
            Assert.Equal("Parent group doesn't exists!", exception.Message);
        }

        [Fact]
        public void RemoveChildGroupWithNullChildGroupShouldThrowAnException()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);

            var group = new Group
            {
                Name = "SoftUni"
            };

            db.Groups.Add(group);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            void Act() => groupService.RemoveChildGroup(group, null);

            // Assert
            var exception = Assert.Throws<ArgumentException>((Action)Act);
            Assert.Equal("Child group doesn't exists!", exception.Message);
        }

        [Fact]
        public void RemoveChildGroupWithGroupThatIsntChildShouldThrowAnException()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);

            var childGroup = new Group
            {
                Name = "Training"
            };

            var group = new Group
            {
                Name = "SoftUni"
            };

            db.Groups.Add(group);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            void Act() => groupService.RemoveChildGroup(group, childGroup);

            // Assert
            var exception = Assert.Throws<ArgumentException>((Action)Act);
            Assert.Equal("This group doesn't have child group with this name!", exception.Message);
        }

        [Fact]
        public void RemoveChildGroupWithCorrectDataShouldRemoveChildGroupFromDatabase()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            var db = new SuasDbContext(dbOptions);

            var childGroup = new Group
            {
                Name = "Training"
            };

            var group = new Group
            {
                Name = "SoftUni",
                ChildGroups = new List<Group>
                {
                    childGroup
                }
            };

            db.Groups.Add(group);
            db.SaveChanges();

            var groupService = new GroupService(db);

            // Act
            var isChildGroupRemovedFromParentGroup = groupService.RemoveChildGroup(group, childGroup);

            // Assert
            Assert.True(isChildGroupRemovedFromParentGroup);
        }
    }
}
