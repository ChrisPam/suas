﻿namespace SUAS.Tests.Services
{
    using System;
    using Data;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using Models;
    using SUAS.Services;
    using Xunit;

    public class RoleServiceTest
    {
        [Fact]
        public void GetAllRolesShouldOrderByCreateDate()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            var db = new SuasDbContext(dbOptions);

            var firstRole = new Role
            {
                Name = "Administrator",
                CreatedDate = DateTime.Now.AddHours(1)
            };

            var secondRole = new Role
            {
                Name = "User",
                CreatedDate = DateTime.Now
            };

            db.Roles.Add(firstRole);
            db.Roles.Add(secondRole);
            db.SaveChanges();

            var roleService = new RoleService(db);

            // Act
            var allRoles = roleService.GetAllRoles();

            // Assert
            Assert.Equal(firstRole, allRoles[1]);
            Assert.Equal(secondRole, allRoles[0]);
        }

        [Fact]
        public void GetCountOfUsersInRole()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            var db = new SuasDbContext(dbOptions);

            var role = new Role
            {
                Id = "1",
                Name = "Administrator",
                CreatedDate = DateTime.Now.AddHours(1)
            };

            var firstUser = new ApplicationUser
            {
                Id = "1",
                UserName = "Pesho"
            };

            var secondUser = new ApplicationUser
            {
                Id = "2",
                UserName = "Gosho"
            };

            var ur1 = new IdentityUserRole<string>
            {
                UserId = firstUser.Id,
                RoleId = role.Id
            };

            var ur2 = new IdentityUserRole<string>
            {
                UserId = secondUser.Id,
                RoleId = role.Id
            };

            db.UserRoles.AddRange(ur1, ur2);
            db.SaveChanges();

            var roleService = new RoleService(db);

            // Act
            var countOfUsers = roleService.GetCountOfUsersInRole(role);

            // Assert
            Assert.True(countOfUsers == 2);
        }

        [Fact]
        public void GetRoleById()
        {
            // Arrange
            var dbOptions = new DbContextOptionsBuilder<SuasDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            var db = new SuasDbContext(dbOptions);

            var role = new Role
            {
                Id = "1",
                Name = "Administrator",
                CreatedDate = DateTime.Now.AddHours(1)
            };

            db.Roles.Add(role);
            db.SaveChanges();

            var roleService = new RoleService(db);

            // Act
            var findedRole = roleService.GetRoleById(role.Id);

            // Assert
            Assert.NotNull(findedRole);
        }

    }
}
